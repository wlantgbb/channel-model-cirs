function[cutoffFreq,ChannelGain]=ChannelGainvsCutoffFreq(filename)

%   Logs:
%   30 Aug 19, Mohammad (m.dehghani@ed.ac.uk):
%       Created

load(filename)


f = linspace(0,100e+6,5000);
j = sqrt(-1);
H_VLC = zeros(1,length(f));

%% Frequency Response of Optical CIR
count = 1;
for fx = f
    for t = 1:length(averun2)
        H_VLC(1,count) = H_VLC(1,count) + averun2(t)*exp(-sqrt(-1)*2*pi*fx*(t-1)/1e+9);
    end
    count = count + 1;
end

% Get the magnitude response
magResp = abs(H_VLC).^2;

% Get the list of freq. bins whose magnitude resp is less than
% half of the DC channel gain
listFreq = f(magResp < 0.5*magResp(1));

if ~isempty(listFreq)
    cutoffFreq = listFreq(1);
else
    % assign infinity if there is no cutoff freq.
    cutoffFreq = inf;
end

varargout{1} = H_VLC;
varargout{2} = f;

ChannelGain=abs(H_VLC(1));
end