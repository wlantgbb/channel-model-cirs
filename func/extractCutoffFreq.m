function [cutoffFreq,varargout] = extractCutoffFreq(filename,varargin)
% extractCutoffFreq obtains the 3dB cutoff frequency 
%
%   CUTOFFFREQ = extractCutoffFreq(FILENAME) extracts 
%   the 3dB cutoff frequency from a CIR that is loaded from 
%   FILENAME. FILENAME must be a relative path of a mat file
%   containing averun1 and averun2 variables.
%
%   [CUTOFFFREQ,FREQRESP,F] = extractCutoffFreq(FILENAME) outputs
%   the frequency respon FREQRESP with the frequency bins F in Hz.

%   TODO:
%       - conversion from time domain averun2 to freq. response is too slow

%   Logs:
%   21 Aug 19, ardimas (ardimasandipurwita@outlook.com):
%       Created
%   04 Sep 19, ardimas (ardimasandipurwita@outlook.com):
%       Use freqz
    
    p = inputParser;
    addOptional(p,'is6dB',false,@(x) validateattributes(x,{'logical'},{'scalar'}))
    parse(p,varargin{:});
    is6dB = p.Results.is6dB;

    % load a mat file
    load(filename)

    % %% From this point, the main code follows 802.11bbChannels.m
    % % I'll keep it as it is for now
    % f = linspace(0,100e+6,5000);
    % j = sqrt(-1);
    % H_VLC = zeros(1,length(f));

    % %% Frequency Response of Optical CIR
    % count = 1;
    % for fx = f
    %     for t = 1:length(averun2)
    %         H_VLC(1,count) = H_VLC(1,count) + averun2(t)*exp(-sqrt(-1)*2*pi*fx*(t-1)/1e+9);
    %     end
    %     count = count + 1;
    % end

    %% Replace it with freqz
    % f = linspace(0,100e+6,5000);
    % H_VLC = freqz(averun2,1,f,fs);
    [H_VLC,f] = freqz(averun2,1,2001,1e9);

    % Get the magnitude response
    magResp = abs(H_VLC).^2;

    if is6dB
        % Get the list of freq. bins whose magnitude resp is less than
        % a quarter of the DC channel gain
        listFreq = f(sqrt(magResp) < 0.5*sqrt(max(magResp)));
    else
        % Get the list of freq. bins whose magnitude resp is less than
        % half of the DC channel gain
        listFreq = f(magResp < 0.5*max(magResp));
    end

    if ~isempty(listFreq)
        cutoffFreq = listFreq(1);
    else 
        % assign infinity if there is no cutoff freq.
        cutoffFreq = inf;
    end

    varargout{1} = H_VLC;
    varargout{2} = f;
    varargout{3} = averun2;

end