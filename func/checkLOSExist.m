function [isLOSExist,varargout] = checkLOSExist(led,pd)
% checkLOSExist calculates whether the LOS link between an LED and 
% a PD exists.
%
%   [ISLOSEXIST] = checkLOSExist(LED,PD) calculates whether 
%   the LOS link between an LED and a PD exist. LED and PD are 
%   structs containing their parameters. 
%   
%   LED.LOC or PD.LOC must be 3x1 Euclidean vectors, [X;Y;Z]
%   LED.ORIENTATION or PD.ORIENTATION must be 2x1 vectors, [POLAR;AZIMUTH] in rad

%   Logs:
%   30 Aug 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

% Check fields of the structs
assert(...
    isfield(led,'loc') & ...
    isfield(led,'orientation'))

assert(...
    isfield(pd,'loc') & ...
    isfield(pd,'orientation') & ...
    isfield(pd,'FoV'))

% Check dimensions
validateattributes(led.loc,{'double'},{'size',[3,1]})
validateattributes(pd.loc,{'double'},{'size',[3,1]})
validateattributes(led.orientation,{'double'},{'size',[2,1]})
validateattributes(pd.orientation,{'double'},{'size',[2,1]})
validateattributes(pd.FoV,{'double'},{'size',[1,1]})

% Get elements
polarPD = pd.orientation(1);
azimuthPD = pd.orientation(2);

% Get vectors
distanceVect = led.loc-pd.loc;
orientationVect = [sin(polarPD)*cos(azimuthPD);sin(polarPD)*sin(azimuthPD);cos(polarPD)];

psi = acos(dot(distanceVect,orientationVect)/norm(distanceVect));

isLOSExist = psi < pd.FoV;
varargout{1} = psi;