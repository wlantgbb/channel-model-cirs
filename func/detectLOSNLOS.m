function [HLOS,HNLOS,varargout] = detectLOSNLOS(filename)
% detectLOSNLOS extracts the LOS and NLOS DC channel gain
%
%   [HLOS,HNLOS] = detectLOSNLOS(FILENAME) extracts 
%   the LOS and NLOS DC channel gain.
%   

%   Logs:
%   30 Aug 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

    % load a mat file
    load(filename)

    % Get the index of the first impulse
    idx = find(averun2 ~= 0);
    idx = idx(1);
    [~,idxMax] = max(averun2);

    % Method-1
    %   The detection is carried out by assuming that 
    %   the signal coming from the first reflection comes 
    %   at least 2 ns after the signal coming from the LOS link.
    %   Note that this might be true for most cases in the dataset.
    %   And, it might be wrong for some datasets where the receiver is 
    %   near a wall and faces it.
    %
    %   I tried this. But, apparently this is not good.

    % if averun2(idx+1) == 0
    %     isLOSExist = true;
    % else
    %     isLOSExist = false;
    % end

    % Method-2
    %   Based on the contribution of the power

    contribution = cumsum(abs(averun2).^2)/sum(abs(averun2).^2);
    if idx == idxMax & contribution(idx) > 0.4 % this is based on observations
        isLOSExist = true;
    else
        isLOSExist = false;
    end


    % Seperate signals from LOS and NLOS
    if isLOSExist
        htLOS = zeros(size(averun2)); htLOS(idx) = averun2(idx);
        averun2(idx) = 0;
        htNLOS = averun2;
    else 
        htLOS = zeros(size(averun2));
        htNLOS = averun2;
    end
    % Note that after this point the variable averun2 is not usable


    %% From this point, the main code follows 802.11bbChannels.m
    % I'll keep it as it is for now
    f = linspace(0,100e+6,5000);
    j = sqrt(-1);
    freqRespLOS = zeros(1,length(f));
    freqRespNLOS = zeros(1,length(f));

    %% Frequency Response of Optical CIR
    if isLOSExist
        count = 1;
        for fx = f
            for t = 1:length(htLOS)
                freqRespLOS(1,count) = freqRespLOS(1,count) + htLOS(t)*exp(-sqrt(-1)*2*pi*fx*(t-1)/1e+9);
            end
            count = count + 1;
        end
    end

    count = 1;
    for fx = f
        for t = 1:length(htNLOS)
            freqRespNLOS(1,count) = freqRespNLOS(1,count) + htNLOS(t)*exp(-sqrt(-1)*2*pi*fx*(t-1)/1e+9);
        end
        count = count + 1;
    end

    HLOS = abs(freqRespLOS(1)).^2;
    HNLOS = abs(freqRespNLOS(1)).^2;

    varargout{1} = freqRespLOS;
    varargout{2} = freqRespNLOS;
    varargout{3} = f;
    varargout{4} = htLOS+htNLOS;

    % Note that to validate it you should do:
    % pow2db(abs(freqRespLOS(1)+freqRespNLOS(1)).^2), or
    % HLOS+HNLOS+(2*sqrt(HLOS*HNLOS))


    