% getAllCutoffFreq obtains list of cutoff freqs from all mat files
% 

%   Logs:
%   21 Aug 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

clear
close all

addpath('./../func')

% Instantiate a wati bar dialog
f = waitbar(0,'1',...
    'CreateCancelBtn','delete(f);');

% Get the variable listMatDir
listMatFiles

try
    numOfMatFiles = length(listMatDir); % the number of mat files
    disp(['> There are ', num2str(numOfMatFiles), ' mat files'])

    disp('> extract information ...(wait)')
    cutoffFreqs = zeros(numOfMatFiles,1);
    for idx = 1:numOfMatFiles
        waitbar(idx/numOfMatFiles,f,sprintf('%d of %d',idx,numOfMatFiles))
        % cutoffFreqs(idx) = extractCutoffFreq(listMatDir{idx},'is6dB',false);
        cutoffFreqs(idx) = extractCutoffFreq(listMatDir{idx},'is6dB',true);
    end

    delete(f)

catch ME
    delete(f)
    rethrow(ME)
end

histogram(cutoffFreqs/1e6,'normalization','cdf')
xlabel('Freq. [MHz]')
ylabel('CDF')

disp(['> ', num2str(length(find(cutoffFreqs < 20e6))/numOfMatFiles), ' have cutoff freq. < 20 MHz'])

% Save the results
% save('./../misc/cutoffFreqFromAll.mat')
save('./../misc/cutoffFreqFromAll6dB.mat')