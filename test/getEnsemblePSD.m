%  getEnsemblePSD is used to obtain and ensemble of PSDs of transmitted signal after the DC bias and clipping
%

%   Logs:
%   04 Sep 19, ardimas (ardimasandipurwita@outlook.com):
%       Created
clear
close all

addpath('./../func')
addpath('./../misc')
addpath('./../filter')
addpath('./../tgbb')
addpath('./../simulator')

% A CIR is used as a dummy
load('./../simulation scenario residential/individual cirs/optical cirs/s5/D7/Run1.mat')

numOfIteration = 1e4;
% numOfIteration = 1e0;

cfgHE = wlanHESUConfig;
cfgHE.ChannelBandwidth = 'CBW20';  % Channel bandwidth
cfgHE.NumSpaceTimeStreams = 1;     % Number of space-time streams
cfgHE.NumTransmitAntennas = 1;     % Number of transmit antennas
cfgHE.APEPLength = 1e3;            % Payload length in bytes
cfgHE.ExtendedRange = false;       % Do not use extended range format
cfgHE.Upper106ToneRU = false;      % Do not use upper 106 tone RU
cfgHE.PreHESpatialMapping = false; % Spatial mapping of pre-HE fields
cfgHE.GuardInterval = 0.8;         % Guard interval duration
cfgHE.HELTFType = 4;               % HE-LTF compression mode
cfgHE.ChannelCoding = 'LDPC';      % Channel coding
cfgHE.MCS = 8;                     % Modulation and coding scheme

fs = wlanSampleRate(cfgHE);

% Instantiate the tgbb object
tgbbChannel                     = wlanTGbbChannel;
tgbbChannel.SymbolRate          = fs;
tgbbChannel.SamplesPerSymbol    = 50;
tgbbChannel.WOCIR               = averun2;

stream = RandStream('combRecursive','Seed',1412);
RandStream.setGlobalStream(stream);

% Generate a packet with random PSDU
psduLength = getPSDULength(cfgHE); % PSDU length in bytes
txPSDU = randi([0 1],psduLength*8,1);
tx = wlanWaveformGenerator(txPSDU,cfgHE);

% Add trailing zeros to allow for channel delay
txPad = [tx; zeros(50,cfgHE.NumTransmitAntennas)];

Pxx_sPos = 0;
disp('> ')
for idx = 1:numOfIteration
    % Pass through a fading indoor TGax channel
    rx = tgbbChannel(txPad);
    [Pxx,f] = pwelch(tgbbChannel.sPos,hamming(floor(length(tgbbChannel.sPos)/4)),[],tgbbChannel.fUp,tgbbChannel.SampleRate);
    Pxx_sPos = Pxx + Pxx_sPos; 
    fprintf('%d ', idx);
end
Pxx_sPos = Pxx_sPos/numOfIteration;
fprintf('\n');

save('./../misc/PSDs.mat','f','Pxx_sPos','freqResp_WOCIR','freqResp_RxAFE')

