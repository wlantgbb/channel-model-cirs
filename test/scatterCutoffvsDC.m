
%   Logs:
%   30 Aug 19, Mohammad (m.dehghani@ed.ac.uk):
%       Created

clear
close all

addpath('./../func')

% keywords = {'empty', 'optical'}; 
keywords = {'residential', 'individual', 'optical'}; 

listMatDirFiltered = getListMatDir(keywords);


numOfMatFiles = length(listMatDirFiltered); % the number of mat files

for idx = 1:numOfMatFiles
    
    [cutoffFreq(idx),ChannelGain(idx)]=ChannelGainvsCutoffFreq(listMatDirFiltered{idx});
    
end

figure;
% scatter(cutoffFreq,ChannelGain)

% xlabel('Cutoff Freq. of Channel');
% ylabel('DC Channel Gain');

scatter(cutoffFreq/1e6,pow2db(abs(ChannelGain).^2))
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('DC Channel Gain (dB)');

title(keywords)

% In case the scatter plot doesn't show anything
set(gcf,'Renderer','painters')