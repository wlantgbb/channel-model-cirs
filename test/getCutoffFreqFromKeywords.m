% getCutoffFreqFromKeywords is a test file to get the cutoff frequencies 
%   from certain folders in the dataset.
%
%   Notes:
%       Make sure that you already have the cutoffFreqFromAll.mat under 
%       the misc directory.


%   Logs:
%   21 Aug 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

clear
close all

addpath('./../func')
addpath('./../misc')

load('cutoffFreqFromAll.mat')

%   KEYWORDS must be one of:
%       'all', 'optical', 'enterprise-conference', 
%       'enterprise-office', 'hospital', 'industrial', 
%       'residential', 'individual', 'overall'

% Note that the order matters

keywords = {'residential', 'individual', 'optical'}; 

listMatDirFiltered = getListMatDir(keywords);

[~,idxFiltered] = ismember(listMatDirFiltered,listMatDir);

cutoffFreqFiltered = cutoffFreqs(idxFiltered);

disp('> The keywords are')
disp(keywords)

disp(['> There are ', num2str(length(idxFiltered)), ' mat files'])

disp(['> ', num2str(length(find(cutoffFreqFiltered < 20e6))), ' of ', num2str(length(idxFiltered)), ' CIRs have cutoff freq. < 20 MHz'])