% getAllLOSNLOS obtains list of cutoff freqs from all mat files
% 

%   Logs:
%   21 Aug 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

clear
close all

addpath('./../func')

% Instantiate a wati bar dialog
f = waitbar(0,'1',...
    'CreateCancelBtn','delete(f);');

% Get the variable listMatDir
listMatFiles

try
    numOfMatFiles = length(listMatDir); % the number of mat files
    disp(['> There are ', num2str(numOfMatFiles), ' mat files'])

    disp('> extract information ...(wait)')
    HLOS = zeros(numOfMatFiles,1);
    HNLOS = zeros(numOfMatFiles,1);
    for idx = 1:numOfMatFiles
        waitbar(idx/numOfMatFiles,f,sprintf('%d of %d',idx,numOfMatFiles))
        [HLOS(idx),HNLOS(idx)] = detectLOSNLOS(listMatDir{idx});
    end

    delete(f)

catch ME
    delete(f)
    rethrow(ME)
end


% Save the results
filename = './../misc/HLOSHNLOSFromAll.mat'
disp(['> save the data to ' filename])
save(filename)