%  testAnalysis is a test file to get some statistics from the dataset
%
%   Notes:
%       Make sure that you already have the cutoffFreqFromAll.mat 
%		and HLOSHNLOSFromAll.mat under the misc directory. 

%   Logs:
%   30 Aug 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

clear
close all

addpath('./../func')
addpath('./../misc')

load('cutoffFreqFromAll.mat')
load('HLOSHNLOSFromAll.mat')

%   KEYWORDS must be one of:
%       'all', 'optical', 'empty', 'enterprise-conference', 
%       'enterprise-office', 'hospital', 'industrial', 
%       'residential', 'individual', 'overall'

% Note that the order matters

% keywords = {'empty', 'optical'}; %--> this includes the 'overall' folder where all CIRs are summed. Hence, it is not intuitive.
% keywords = {'enterprise-conference', 'individual', 'optical'}; 
% keywords = {'enterprise-office', 'optical'}; 
% keywords = {'hospital', 'individual', 'optical'}; 
% keywords = {'industrial', 'individual', 'optical'}; 
keywords = {'residential', 'individual', 'optical'}; 

listMatDirFiltered = getListMatDir(keywords);

[~,idxFiltered] = ismember(listMatDirFiltered,listMatDir);

cutoffFreqFiltered = cutoffFreqs(idxFiltered);
HLOSFiltered = HLOS(idxFiltered);
HNLOSFiltered = HNLOS(idxFiltered);

% Metrics
pathloss_dB = abs(pow2db(HLOSFiltered+HNLOSFiltered+(2*sqrt(HLOSFiltered.*HNLOSFiltered))));
ratioNLOStoTotal = HNLOSFiltered./(HNLOSFiltered+HLOSFiltered);
isLOSExist = HLOSFiltered~=0;

% Display
disp('> The keywords are')
disp(keywords)

disp(['> There are ', num2str(length(idxFiltered)), ' mat files'])

disp(['> ', num2str(length(find(cutoffFreqFiltered < 20e6))), ' of ', num2str(length(idxFiltered)), ' CIRs have cutoff freq. < 20 MHz'])

disp(['> ', num2str(length(find(cutoffFreqFiltered == inf))), ' samples have cutoff freq. = inf'])


figure
subplot(1,2,1)
% scatter(cutoffFreqFiltered/1e6,pathloss_dB)
histogram2(cutoffFreqFiltered/1e6,pathloss_dB)
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('Pathloss (dB)');
zlabel('# of samples');
set(gca,'FontSize',20)
% title([keywords, 'there are ', num2str(length(find(cutoffFreqFiltered == inf))), ' more samples where the cutoff freq is inf'])
% annotation('textbox',[.3 .4 .5 .6],'String',[keywords, 'there are ', num2str(length(find(cutoffFreqFiltered == inf))), ' more samples where the cutoff freq is inf'],'FitBoxToText','on');
subplot(1,2,2)
scatter(cutoffFreqFiltered/1e6,pathloss_dB)
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('Pathloss (dB)');
set(gca,'FontSize',20)

% In case the scatter plot doesn't show anything
% set(gcf,'Renderer','painters')

% Following is used if I am confident enough with the LOS link 
% detection problem from CIR only

% figure
% scatter(cutoffFreqFiltered/1e6,ratioNLOStoTotal)
% xlabel('Cutoff Freq. of Channel (MHz)');
% ylabel('HNLOS/(HLOS+HNLOS)');

% % In case the scatter plot doesn't show anything
% set(gcf,'Renderer','painters')

% figure
% scatter(ratioNLOStoTotal,pathloss_dB)
% xlabel('HNLOS/(HLOS+HNLOS)');
% ylabel('Pathloss (dB)');

% % In case the scatter plot doesn't show anything
% set(gcf,'Renderer','painters')

% figure
% scatter(isLOSExist,pathloss_dB)
% xlabel('LOS exists?');
% ylabel('Pathloss (dB)');

% % In case the scatter plot doesn't show anything
% set(gcf,'Renderer','painters')

% figure
% scatter(isLOSExist,ratioNLOStoTotal)
% xlabel('LOS exists?');
% ylabel('HNLOS/(HLOS+HNLOS)');

% % In case the scatter plot doesn't show anything
% set(gcf,'Renderer','painters')

% The existence of LOS link has nothing to do with the cutoff freq.
% figure
% scatter(cutoffFreqFiltered/1e6,isLOSExist)
% xlabel('Cutoff Freq. of Channel (MHz)');
% ylabel('LOS exists?');

% % In case the scatter plot doesn't show anything
% set(gcf,'Renderer','painters')

