%  getPathlossFromKeywords is used to get the statistics of received electrical SNRs
%
%   Notes:
%       Make sure that you already have the cutoffFreqFromAll.mat 
%        and HLOSHNLOSFromAll.mat under the misc directory. 

%   Logs:
%   04 Sep 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

clear
close all

addpath('./../func')
addpath('./../misc')

% load('cutoffFreqFromAll.mat')
load('cutoffFreqFromAll6dB.mat')
load('HLOSHNLOSFromAll.mat')

%   KEYWORDS must be one of:
%       'all', 'optical', 'empty', 'enterprise-conference', 
%       'enterprise-office', 'hospital', 'industrial', 
%       'residential', 'individual', 'overall'

% Note that the order matters

keywords = {'empty', 'optical'}; %--> this includes the 'overall' folder where all CIRs are summed. Hence, it is not intuitive.
% keywords = {'enterprise-conference', 'individual', 'optical'}; 
% keywords = {'enterprise-office', 'optical'}; 
% keywords = {'hospital', 'individual', 'optical'}; 
% keywords = {'industrial', 'individual', 'optical'}; 
% keywords = {'residential', 'individual', 'optical'}; 

listMatDirFiltered = getListMatDir(keywords);

[~,idxFiltered] = ismember(listMatDirFiltered,listMatDir);

cutoffFreqFiltered = cutoffFreqs(idxFiltered);

HLOSFiltered = HLOS(idxFiltered);
HNLOSFiltered = HNLOS(idxFiltered);
pathloss_dB = abs(pow2db(HLOSFiltered+HNLOSFiltered+(2*sqrt(HLOSFiltered.*HNLOSFiltered))));

% Display
disp('> The keywords are')
disp(keywords)

disp(['> There are ', num2str(length(idxFiltered)), ' mat files'])

disp(['> ', num2str(length(find(cutoffFreqFiltered < 20e6))), ' of ', num2str(length(idxFiltered)), ' CIRs have cutoff freq. < 20 MHz'])

disp(['> ', num2str(length(find(cutoffFreqFiltered == inf))), ' samples have cutoff freq. = inf'])


figure
subplot(2,2,1)
histogram2(cutoffFreqFiltered/1e6,pathloss_dB)
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('Pathloss (dB)');
zlabel('# of samples');
set(gca,'FontSize',16)

subplot(2,2,2)
scatter(cutoffFreqFiltered/1e6,pathloss_dB)
hold on
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('Pathloss (dB)');
set(gca,'FontSize',16)
set(gcf,'Renderer','painters')

subplot(2,2,3:4)
histogram(pathloss_dB(find(cutoffFreqFiltered == inf)))
xlabel('Pathloss (dB)');
ylabel('# of samples');
title('Pathloss (dB) of channels whose cutoff freq = inf')
set(gca,'FontSize',16)

% To make sure the axis limits are adjusted properly
subplot(2,2,1)
a = gca;
xlim([0,a.XLim(2)])

% To make sure the axis limits are adjusted properly
% and the 20 MHz line 
subplot(2,2,2)
a = gca;
xlim([0,a.XLim(2)])
ylim([a.YLim(1),a.YLim(2)])
plot([20,20],[a.YLim(1),a.YLim(2)],'--k')

if any(contains(keywords,'empty','IgnoreCase',true))
    [X,Y] = meshgrid(1:1:10,1:1:10);
    cutoffMatrixMax = zeros(10,10);
    cutoffMatrixMin = zeros(10,10);

    %idxCell --> column
    %idxDummy --> row
    for idxCell = 1:10
        for idxDummy = 1:10
            idx = find(contains(listMatDirFiltered,['cell-' num2str(idxCell) '/' num2str(idxDummy) '/'],'IgnoreCase',true));
            cutoffMax = max(cutoffFreqFiltered(idx));
            cutoffMin = min(cutoffFreqFiltered(idx));
            cutoffMatrixMax(idxCell,idxDummy) = cutoffMax;
            cutoffMatrixMin(idxCell,idxDummy) = cutoffMin;
        end
    end

    figure
    subplot(1,2,1)
    bar3(cutoffMatrixMin/1e6)
    a = gca;
    xlabel('First subscript (Cell Idx)')
    ylabel('Second subscript')
    zlabel('Min Cutoff Freq. (MHz)')
    set(gca,'FontSize',16)

    subplot(1,2,2)
    cutoffMatrixMax(find(cutoffMatrixMax==inf)) = 1e9/2; 
    bar3(cutoffMatrixMax/1e6)
    zlim([0,500])
    xlabel('First subscript (Cell Idx)')
    ylabel('Second subscript')
    zlabel('Max Cutoff Freq. (MHz)')
    set(gca,'FontSize',16)

    [X,Y] = meshgrid(1:1:10,1:1:10);
    whichPDMax = zeros(10,10);
    whichPDMin = zeros(10,10);

    %idxCell --> column
    %idxDummy --> row
    for idxCell = 1:10
        for idxDummy = 1:10
            idx = find(contains(listMatDirFiltered,['cell-' num2str(idxCell) '/' num2str(idxDummy) '/'],'IgnoreCase',true));
            [~,idxMax] = max(cutoffFreqFiltered(idx));
            [~,idxMin] = min(cutoffFreqFiltered(idx));
                
            if contains(listMatDirFiltered(idx(idxMax)),'D1/','IgnoreCase',true)
                    whichPDMax(idxCell,idxDummy) = 1;
            elseif contains(listMatDirFiltered(idx(idxMax)),'D2/','IgnoreCase',true)
                    whichPDMax(idxCell,idxDummy) = 2;
            elseif contains(listMatDirFiltered(idx(idxMax)),'D3/','IgnoreCase',true)
                    whichPDMax(idxCell,idxDummy) = 3;
            elseif contains(listMatDirFiltered(idx(idxMax)),'D4/','IgnoreCase',true)
                    whichPDMax(idxCell,idxDummy) = 4;
            elseif contains(listMatDirFiltered(idx(idxMax)),'D5/','IgnoreCase',true)
                    whichPDMax(idxCell,idxDummy) = 5;
            elseif contains(listMatDirFiltered(idx(idxMax)),'D6/','IgnoreCase',true)
                    whichPDMax(idxCell,idxDummy) = 6;
            elseif contains(listMatDirFiltered(idx(idxMax)),'D7/','IgnoreCase',true)
                    whichPDMax(idxCell,idxDummy) = 7;
            end

            if contains(listMatDirFiltered(idx(idxMin)),'D1/','IgnoreCase',true)
                    whichPDMin(idxCell,idxDummy) = 1;
            elseif contains(listMatDirFiltered(idx(idxMin)),'D2/','IgnoreCase',true)
                    whichPDMin(idxCell,idxDummy) = 2;
            elseif contains(listMatDirFiltered(idx(idxMin)),'D3/','IgnoreCase',true)
                    whichPDMin(idxCell,idxDummy) = 3;
            elseif contains(listMatDirFiltered(idx(idxMin)),'D4/','IgnoreCase',true)
                    whichPDMin(idxCell,idxDummy) = 4;
            elseif contains(listMatDirFiltered(idx(idxMin)),'D5/','IgnoreCase',true)
                    whichPDMin(idxCell,idxDummy) = 5;
            elseif contains(listMatDirFiltered(idx(idxMin)),'D6/','IgnoreCase',true)
                    whichPDMin(idxCell,idxDummy) = 6;
            elseif contains(listMatDirFiltered(idx(idxMin)),'D7/','IgnoreCase',true)
                    whichPDMin(idxCell,idxDummy) = 7;
            end
                
        end
    end

    figure
    subplot(1,2,1)
    bar3(whichPDMin)
    a = gca;
    xlabel('First subscript (Cell Idx)')
    ylabel('Second subscript')
    zlabel('Which PD gives min')
    set(gca,'FontSize',16)

    subplot(1,2,2)
    bar3(whichPDMax)
    xlabel('First subscript (Cell Idx)')
    ylabel('Second subscript')
    zlabel('Which PD gives max')
    set(gca,'FontSize',16)
end




