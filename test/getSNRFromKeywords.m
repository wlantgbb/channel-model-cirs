%  getSNRFromKeywords is used to get the statistics of received electrical SNRs
%
%   Notes:
%       Make sure that you already have the cutoffFreqFromAll.mat 
%		and PSDs.mat under the misc directory. 

%   Logs:
%   04 Sep 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

clear
close all

addpath('./../func')
addpath('./../misc')

load('cutoffFreqFromAll.mat')
load('PSDs.mat')

%   KEYWORDS must be one of:
%       'all', 'optical', 'empty', 'enterprise-conference', 
%       'enterprise-office', 'hospital', 'industrial', 
%       'residential', 'individual', 'overall'

% Note that the order matters

% keywords = {'empty', 'optical'}; %--> this includes the 'overall' folder where all CIRs are summed. Hence, it is not intuitive.
% keywords = {'enterprise-conference', 'individual', 'optical'}; 
% keywords = {'enterprise-office', 'optical'}; 
% keywords = {'hospital', 'individual', 'optical'}; 
% keywords = {'industrial', 'individual', 'optical'}; 
keywords = {'residential', 'individual', 'optical'}; 

if ismember('empty',keywords)
	Ptx = 11; % in Watt
elseif ismember('enterprise-conference',keywords)
	Ptx = 46; % in Watt
elseif ismember('enterprise-office',keywords)
	Ptx = 52; % in Watt
elseif ismember('hospital',keywords)
	Ptx = 19; % in Watt
elseif ismember('industrial',keywords)
	Ptx = 1; % in Watt
elseif ismember('residential',keywords)
	Ptx = 12; % in Watt
end

listMatDirFiltered = getListMatDir(keywords);

[~,idxFiltered] = ismember(listMatDirFiltered,listMatDir);

cutoffFreqFiltered = cutoffFreqs(idxFiltered);

% Instantiate a waiting bar dialog
waitBar = waitbar(0,'1',...
    'CreateCancelBtn','delete(waitBar);');

% receivedElecSNRdB is the SNR after the Rx FE
receivedElecSNRdB = zeros(size(listMatDirFiltered));

try
    for idx = 1:length(listMatDirFiltered)
    	load(listMatDirFiltered{idx});

    	% f is from PSDs.mat
    	% 1e9 means sampling freq of 1 Gsa/s
    	freqResp_WOCIR = freqz(averun2,1,f,1e9);

    	SNR = (Ptx/trapz(f,Pxx_sPos))*trapz(f,Pxx_sPos.*(abs(freqResp_WOCIR.*freqResp_RxAFE)).^2)/(db2pow(-204)*40e6);
    	receivedElecSNRdB(idx) = pow2db(SNR);

        waitbar(idx/length(listMatDirFiltered),waitBar,sprintf('%d of %d',idx,length(listMatDirFiltered)))
    end

    delete(waitBar);

catch ME
    delete(waitBar)
    rethrow(ME)
end

% Display
disp('> The keywords are')
disp(keywords)

disp(['> There are ', num2str(length(idxFiltered)), ' mat files'])

disp(['> ', num2str(length(find(cutoffFreqFiltered < 20e6))), ' of ', num2str(length(idxFiltered)), ' CIRs have cutoff freq. < 20 MHz'])

disp(['> ', num2str(length(find(cutoffFreqFiltered == inf))), ' samples have cutoff freq. = inf'])


figure
subplot(2,2,1)
histogram2(cutoffFreqFiltered/1e6,receivedElecSNRdB)
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('Rx elec. SNR (dB)');
zlabel('# of samples');
set(gca,'FontSize',16)

subplot(2,2,2)
scatter(cutoffFreqFiltered/1e6,receivedElecSNRdB)
hold on
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('Rx elec SNR (dB)');
set(gca,'FontSize',16)
set(gcf,'Renderer','painters')

subplot(2,2,3:4)
histogram(receivedElecSNRdB(find(cutoffFreqFiltered == inf)))
xlabel('Rx elec. SNR (dB)');
ylabel('# of samples');
title('Rx elec SNR (dB) of channels whose cutoff freq = inf')
set(gca,'FontSize',16)

% To make sure the axis limits are adjusted properly
subplot(2,2,1)
a = gca;
xlim([0,a.XLim(2)])

% To make sure the axis limits are adjusted properly
% and the 20 MHz line 
subplot(2,2,2)
a = gca;
xlim([0,a.XLim(2)])
ylim([a.YLim(1),a.YLim(2)])
plot([20,20],[a.YLim(1),a.YLim(2)],'--k')
