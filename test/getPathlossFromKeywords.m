%  getPathlossFromKeywords is used to get the statistics of received electrical SNRs
%
%   Notes:
%       Make sure that you already have the cutoffFreqFromAll.mat 
%		and HLOSHNLOSFromAll.mat under the misc directory. 

%   Logs:
%   04 Sep 19, ardimas (ardimasandipurwita@outlook.com):
%       Created

clear
close all

addpath('./../func')
addpath('./../misc')

% load('cutoffFreqFromAll.mat')
load('cutoffFreqFromAll6dB.mat')
load('HLOSHNLOSFromAll.mat')

%   KEYWORDS must be one of:
%       'all', 'optical', 'empty', 'enterprise-conference', 
%       'enterprise-office', 'hospital', 'industrial', 
%       'residential', 'individual', 'overall'

% Note that the order matters

keywords = {'empty', 'optical'}; %--> this includes the 'overall' folder where all CIRs are summed. Hence, it is not intuitive.
% keywords = {'enterprise-conference', 'individual', 'optical'}; 
% keywords = {'enterprise-office', 'optical'}; 
% keywords = {'hospital', 'individual', 'optical'}; 
% keywords = {'industrial', 'individual', 'optical'}; 
% keywords = {'residential', 'individual', 'optical'}; 

listMatDirFiltered = getListMatDir(keywords);

[~,idxFiltered] = ismember(listMatDirFiltered,listMatDir);

cutoffFreqFiltered = cutoffFreqs(idxFiltered);

HLOSFiltered = HLOS(idxFiltered);
HNLOSFiltered = HNLOS(idxFiltered);
pathloss_dB = abs(pow2db(HLOSFiltered+HNLOSFiltered+(2*sqrt(HLOSFiltered.*HNLOSFiltered))));

% Display
disp('> The keywords are')
disp(keywords)

disp(['> There are ', num2str(length(idxFiltered)), ' mat files'])

disp(['> ', num2str(length(find(cutoffFreqFiltered < 20e6))), ' of ', num2str(length(idxFiltered)), ' CIRs have cutoff freq. < 20 MHz'])

disp(['> ', num2str(length(find(cutoffFreqFiltered == inf))), ' samples have cutoff freq. = inf'])


figure
subplot(2,2,1)
histogram2(cutoffFreqFiltered/1e6,pathloss_dB)
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('Pathloss (dB)');
zlabel('# of samples');
set(gca,'FontSize',16)

subplot(2,2,2)
scatter(cutoffFreqFiltered/1e6,pathloss_dB)
hold on
xlabel('Cutoff Freq. of Channel (MHz)');
ylabel('Pathloss (dB)');
set(gca,'FontSize',16)
set(gcf,'Renderer','painters')

subplot(2,2,3:4)
histogram(pathloss_dB(find(cutoffFreqFiltered == inf)))
xlabel('Pathloss (dB)');
ylabel('# of samples');
title('Pathloss (dB) of channels whose cutoff freq = inf')
set(gca,'FontSize',16)

% To make sure the axis limits are adjusted properly
subplot(2,2,1)
a = gca;
xlim([0,a.XLim(2)])

% To make sure the axis limits are adjusted properly
% and the 20 MHz line 
subplot(2,2,2)
a = gca;
xlim([0,a.XLim(2)])
ylim([a.YLim(1),a.YLim(2)])
plot([20,20],[a.YLim(1),a.YLim(2)],'--k')
