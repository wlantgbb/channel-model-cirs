classdef wlanTGbbChannel < matlab.System
%wlanTGbbChannel filters input signal based on TGbb specification
%Note that this is a miniature of a more complete function which whill be published 
%on another link.
%
%   tgbb = wlanTGbbChannel creates an object that follows TGbb specifications [1]. 
%   Details of it are provided in the methodology document as found in [2]. Initial 
%   simulation results are also presented in [3].
%
%   Step method syntax:
%
%   Y = step(tgbb,x) filter input signal X according to the TGbb sepcification.s
%   
%   wlanTGbbChannel properties:
%
%   SymbolRate          - Input signal symbol rate (symbols/sec)
%   SamplesPerSymbol    - Samples per symbol for upconversion 
%   Offset              - Freq. offset for the upconversion
%   beta                - Rollof factor for the pulse shaping
%   span                - The number of spanned symbols
%   ClippingRatio       - The clipping ratio for the DC bias
%   
%   % References:
%   % [1] http://www.ieee802.org/11/Reports/tgbb_update.htm
%   % [2] https://mentor.ieee.org/802.11/dcn/19/11-19-0187-04-00bb-evaluation-methodology-for-phy-and-mac-proposals.docx
%   % [3] https://mentor.ieee.org/802.11/dcn/19/11-19-1224-01-00bb-simulation-results-for-802-11a-phy-in-lc.ppt
%
%   See also wlanTGnChannel, wlanTGacChannel, wlanTGahChannel.
%

%   TODO:
%       - The most important thing is to make sure that the steps follow the TGbb spec
%       - Adds an example in the documentation
%       - Define a local random generator seed
%       - Pulse shaping should be replaced by a DAC and an ADC models, especially for high sample rate

%   Logs:
%   16 Aug 19, ardimas (ardimasandipurwita@outlook.com):
%       Created



properties (Nontunable)
    %Symbol rate (symbols/sec)
    SymbolRate = 20e6;
    %Samples per symbol 
    %   This is used the continuous-time simulation
    %   '11-19-0187-04-00bb-evaluation-methodology-for-phy-and-mac-proposals.docx' 
    %   suggest that the sample rate is 1 Gsamp/s. However, using 
    %   the default value of symbol rate, it needs 50 samples per symbol.
    %   This will lengthen the simulation time. I tried 4, 6, 8, 10, 20 and 50.
    %   The optimal value (trade-off between performance and runtime) is 10.
    SamplesPerSymbol = 10;
    %Offset (Hz)
    %   This is the offset for the upconversion
    %   see 11-19-0187-04-00bb-evaluation-methodology-for-phy-and-mac-proposals
    Offset = 1.5e6;
    %Rolloff factor
    %   This is for the pulse shaping using the raised cosine
    beta = 0.5;
    %Span
    %   Again this is for the pulse shaping using the raised cosine
    span = 8;
    %Clipping ratio
    %   Clipping ratio is used to determine the DC bias, i.e., DC = clipping ratio*std(sTx)
    ClippingRatio = 3;
    %WOCIR
    %   wireless optical CIR from the dataset, i.e.,
    %   https://mentor.ieee.org/802.11/dcn/18/11-18-1603-01-00bb-cirs-of-ieee-802-11bb-reference-channel-models.zip
    WOCIR
end

properties (Nontunable)
    %Packet length or the number of symbols in a packet
    lenPacket
    %Sampling freq. (samples/sec)
    SampleRate
    %CIR of the raised cosine
    cirRcos
    %Freq. center of the upconversion
    Fc
    %Time bins at downsampled
    tDown
    %Time bins at upsampled
    tUp
    %Freq bins at downsampled
    fDown
    %Freq bins at upsampled
    fUp
    %Tx AFE
    %   see 11-18-1574-04-00bb-lc-frontend-models
    fetx
    %Rx AFE
    %   see 11-18-1574-04-00bb-lc-frontend-models
    ferx
end

properties 
    %Ptx: Optical power in Watt
    Ptx = 1
    %Signal input
    sInput
    %Signal after upsampling
    sUpsampled
    %Signal after upsampling+pulse shape
    sPulseShaped
    %Signal after upconversion
    sUpconverted
    %Signal after AFE Tx
    sTx
    %Signal after DC bias
    sPos
    %Signal after WOCIR
    sWOCIR
    %Signal after AWGN
    sNoise
    %Signal after AFE Rx
    sRx
    %Freq responses
    freqResp_cirRcos
    freqResp_TxAFE
    freqResp_RxAFE
    freqResp_WOCIR
    %FIR filters
    firPS
    firTxAFE
    firRxAFE
    firWOCIR
    %Scaling factor
    sfPulseShaped = 1
    sfUpConverted = 1
    sfAGC = 1
    sfDownConverted = 1
    sfMatchedFiltered = 1
    sfDownSampled = 1
end

methods
    function obj = wlanTGbbChannel(varargin) % Constructor
        setProperties(obj, nargin, varargin{:});
    end 

    % function visualize(obj,varargin)
    % %Visualize all time domains and PSDs of signals of interest.    

    % end

end

methods(Access = protected)
    function setupImpl(obj,varargin)

        disp('> Setup filter coefficients')
        
        % Get the packet length
        obj.lenPacket = length(varargin{1});

        % Sampling rate
        obj.SampleRate = obj.SymbolRate*obj.SamplesPerSymbol;

        % CIR for the raised cosine filter
        obj.cirRcos = rcosdesign(obj.beta,obj.span,obj.SamplesPerSymbol);
        obj.firPS.num = obj.cirRcos;
        obj.firPS.den = 1;

        % Freq. center for the upconversion
        obj.Fc = obj.SymbolRate/2 + obj.Offset;
        
        % Get the time and freq. bins
        [obj.fDown,obj.tDown] = freq_time(obj.lenPacket,obj.SymbolRate);
        [obj.fUp,obj.tUp] = freq_time(obj.lenPacket*obj.SamplesPerSymbol,obj.SampleRate);

        % Tx AFE
        obj.fetx = choose_filter('fetx');

        % Rx AFE
        obj.ferx = choose_filter('ferx');

        % The Tx AFE is given with the assumption that the freq. sampling is 1 Gsa/s.
        % So, if we want to simulate with sampling rate less than 1 Gsa/s, we need to find 
        % the respective CIR.
        if obj.SampleRate ~= 1e9
            
            %Tx AFE
            freqResp_TxAFE = (freqz(obj.fetx.num,obj.fetx.den,obj.fUp,1e9));
            %Rx AFE
            freqResp_RxAFE = (freqz(obj.ferx.num,obj.ferx.den,obj.fUp,1e9));

            obj.firTxAFE.num = getCIRFromFreqResp(freqResp_TxAFE);
            obj.firTxAFE.den = 1;
            obj.firRxAFE.num = getCIRFromFreqResp(freqResp_RxAFE);
            obj.firRxAFE.den = 1;

            % WOCIR
            fs = 1e9;
            [h,f_data] = freqz(obj.WOCIR,1,2001,'whole',fs);
            freqResp_data = (h);

            fitObjR=fit(f_data(:),real(freqResp_data(:)),'smoothingspline'); % real
            fitObjI=fit(f_data(:),imag(freqResp_data(:)),'smoothingspline'); % imag
            freqResp_WOCIR = arrayfun(@(f) feval(fitObjR,abs(f))+1i*feval(fitObjI,abs(f)),obj.fUp);

            obj.firWOCIR.num = getCIRFromFreqResp(freqResp_WOCIR);
            obj.firWOCIR.den = 1;

        else 
            obj.firTxAFE.num = obj.fetx.num;
            obj.firTxAFE.den = obj.fetx.den;
            obj.firRxAFE.num = obj.ferx.num;
            obj.firRxAFE.den = obj.ferx.den;

            obj.firWOCIR.num = obj.WOCIR;
            obj.firWOCIR.den = 1;
        end


        % Freq. response
        % WOCIR
        obj.freqResp_WOCIR = freqz(obj.WOCIR,1,obj.fUp,1e9);
        
        %Rx AFE
        obj.freqResp_RxAFE = (freqz(obj.ferx.num,obj.ferx.den,obj.fUp,1e9));

    end

    function varargout = stepImpl(obj,input)

        % I will repeatedly use a temporary variable x
        x = input;
        obj.sInput = x;

        % Upsampling 
        x = upsample(x,obj.SamplesPerSymbol);
        obj.sUpsampled = x;

        % Pulse shaping
        x = filter(obj.firPS.num,obj.firPS.den,x);
        [x,obj.sfPulseShaped] = normalize(x,input); 
        obj.sPulseShaped = x;

        % Upconversion
        x = upConversion(x,obj.Fc,obj.tUp);
        [x,obj.sfUpConverted] = normalize(x,obj.sPulseShaped); 
        obj.sUpconverted = x;

        % Tx AFE
        x = filter(obj.firTxAFE.num,obj.firTxAFE.den,x);
        obj.sTx = x;

        % DC bias
        x = x+obj.ClippingRatio*sqrt(var(x));

        % Clip
        x(x<0) = 0; 

        % Adjust the optical power
        x = x*sqrt(obj.Ptx/mean(abs(x).^2));
        obj.sPos = x;

        % Wireless optical CIR
        x = filter(obj.firWOCIR.num,obj.firWOCIR.den,x);
        obj.sWOCIR = x;

        % Rx AFE
        x = filter(obj.firRxAFE.num,obj.firRxAFE.den,x);
        obj.sRx = x;

        varargout{1} = obj.sRx;


    end
end

end

function freqResp = getfreqResp(num,den,f,fs,varargin)

    p = inputParser;
    addOptional(p,'isIncludeDelay',false,@(x) validateattributes(x,{'logical'},{'scalar'}))
    parse(p,varargin{:});
    isIncludeDelay = p.Results.isIncludeDelay;

    if isIncludeDelay
        freqResp = (freqz(num,den,f,fs));
    else
        freqResp = abs(freqz(num,den,f,fs)); % ignore the phase response
    end
end

function y = filtering(x,freqResp)
    %Filters the input signal x according to a frequency response
    % Doing it this way is more flexible in terms of setting whether 
    % we want to include the phase distortion (delay) or not.
    y = ifft( fft(x).* ifftshift( freqResp ) );
end

function y = filteringReal(x,freqResp)
    %The output is real. Hence, the symmetric argument.
    y = ifft( fft(x).* ifftshift( freqResp ), 'symmetric' );
end

function [y,scalingFactor] = normalize(x,xRef)
    %Normalize so that the energy of x is equal the energy of xRef
    scalingFactor = sqrt(mean(abs(xRef).^2)/mean(abs(x).^2));
    y = x*scalingFactor;

end

function y = upConversion(x,fc,t)
    y = real(x).*cos(2*pi*fc*t)+imag(x).*sin(2*pi*fc*t);
    y = real(y); % make sure that the output is real
end

function y = downConversion(x,fc,t)
    y = x.*cos(2*pi*fc*t)+1i*x.*sin(2*pi*fc*t);
end

function cir = getCIRFromFreqResp(freqResp,varargin)
    %Obtain the time-domain CIR from a frequency response
    % This function is useful especially if the given frequency response is 
    % generated with sampling frequency that is not the same as the simulated one.

    getRealCIR = @(freqResp) ifft( ifftshift( freqResp ), 'symmetric' );
    getCmplxCIR = @(freqResp) ifft( ifftshift( freqResp ) );

    p = inputParser;
    addOptional(p,'isReal',true,@(x) validateattributes(x,{'logical'},{'scalar'}))
    parse(p,varargin{:});
    isReal = p.Results.isReal;

    if isReal
        cir = getRealCIR(freqResp);
    else
        cir = getCmplxCIR(freqResp);
    end

    % Choose the significant ones
    threshold = 1-1e-6;      % energy threshold above which impulse respone is neglected
    E = cumsum(abs(cir).^2)/sum(abs(cir).^2);
    cir = cir(E < threshold);

end



