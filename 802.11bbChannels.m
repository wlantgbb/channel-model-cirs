close all
clear all
clc

%% LED and Channel Parameters
f_cut_off = 20e+6; % Cut-off frequency of LED
t = 0:1e-9:200*1e-9; % Time duration of channel impulse response (CIR)

%% Associated CIR
load('Run1') 

%% Frequency Responses of Different LED Models
f = linspace(0,100e+6,5000);
j = sqrt(-1);
H_LED = 1./(1+j*f/f_cut_off); % "LED Model 1"
% H_LED = exp((-log(sqrt(2)))*((f/f_cut_off).^2)); % "LED Model 2"
H_VLC = zeros(1,length(f));

%% Frequency Response of Optical CIR
count = 1;
for fx = f
    for t = 1:length(averun2)
        H_VLC(1,count) = H_VLC(1,count) + averun2(t)*exp(-sqrt(-1)*2*pi*fx*(t-1)/1e+9);
    end
    count = count + 1;
end

figure (1)
plot(f/1e+6,pow2db(abs(H_VLC).^2),'linewidth',2)
grid on
xlabel('Frequency [MHz]')
ylabel('|\itH\rm(\itf\rm)|^{2} [dB]')

%% Frequency Response of Effective CIR
H_VLCeff(1,:) = H_VLC(1,:).*H_LED; 

figure (2)
plot(f/1e+6,pow2db(abs(H_VLCeff).^2),'linewidth',2)
grid on
xlabel('Frequency [MHz]')
ylabel('|\itH\rm_{eff}\rm(\itf\rm)|^{2} [dB]')

%% Effective CIR in Time Domain
z=1;
f_cut_off = 20e+6; % Cut-off frequency of LED
t = 0:1e-9:200*1e-9; % Time duration of channel impulse response (CIR)

% "LED Model 1" in time domain
dummy = exp(-f_cut_off*(2*pi)*t);
LED_Model_1_Response = dummy/norm(dummy);

averun2 = conv(averun2,LED_Model_1_Response);
averun1 = 1:length(averun2);

averun2=averun2';
averun1=averun1';

% Effective CIR
filename = ['Run' num2str(z) '.mat'];
save(filename,'averun1','averun2');
