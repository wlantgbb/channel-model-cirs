# channel-model-cirs
Contains scraping codes for the TGbb reference channel models. 

The dataset is retrieved from [[1]](https://mentor.ieee.org/802.11/dcn/18/11-18-1603-01-00bb-cirs-of-ieee-802-11bb-reference-channel-models.zip). Its documentation is avalaible on [[2]](https://mentor.ieee.org/802.11/dcn/18/11-18-1582-04-00bb-ieee-802-11bb-reference-channel-models-for-indoor-environments.pdf). 

## Getting Started

### Obtaining the list of mat files
A straightforward way to get the list of all mat files is by running following command in your terminal. 

```sh
find . -iname "*simulation*" | xargs -I{} find {} -type f -name '*.mat' > list_folders.txt
```

From the txt file, you can convert it to proper MATLAB syntax. The result should be saved under the directory 'func', name the file as listMatFiles.m with the variable name listMatDir. 

## Contributors

Any changes should be done under the func, misc, result or test folders only. I want to keep the other folder fixed and originals.

## TODOs

- Integrating with Mohammad's function of detecting LOS and NLOS

## Authors

* **Ardimas Purwita** - *Initial work* - ardimasandipurwita@outlook.com

## Contributors

* **Mohammad Soltani** - m.dehghani@ed.ac.uk

## References

[1] https://mentor.ieee.org/802.11/dcn/18/11-18-1603-01-00bb-cirs-of-ieee-802-11bb-reference-channel-models.zip

[2] https://mentor.ieee.org/802.11/dcn/18/11-18-1582-04-00bb-ieee-802-11bb-reference-channel-models-for-indoor-environments.pdf
